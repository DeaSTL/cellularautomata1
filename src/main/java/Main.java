import processing.core.PApplet;
import java.util.Random;

public class Main  extends PApplet {
    public int screenWidth = 800;
    public int screenHeight = 800;
    public Random random = new Random();
    public float[][] matrix = new float[screenWidth][screenHeight];
    int spread = 1;
    int spreadDeviation = 3;
    float reproductionThreshold = 0.10f;
    public static void main(String[] args){
        PApplet.main("Main");
    }
    public void start(){
        size(screenWidth,screenHeight);

    }
    public void setup(){
        noStroke();
        colorMode(HSB, 360, 100, 100);
    }

    public void draw(){

        for(int i = 1;i<width;i++){
            for(int j = 1;j<height;j++){
                int c = 0;
                fill(0);
                set(i,j,0);
                if(matrix[i][j] != 0 && matrix[i][j] > 0) {
                    set(i,j,color(matrix[i][j] * 50,matrix[i][j] * 100,matrix[i][j] * 100));
                }
            }
        }
        if(frameCount % 2 == 1) {
            for (int c = 0; c < 1; c++) {
                float[][] tmpMatrix = new float[width][height];
                for (int i = 0; i < width; i++) {
                    for (int j = 0; j < height; j++) {
                        tmpMatrix[i][j] = matrix[i][j];
                    }
                }

                for (int i = spread + spreadDeviation; i < width - spread - spreadDeviation; i++) {
                    for (int j = spread + spreadDeviation; j < height - spread - spreadDeviation; j++) {
                        if (matrix[i][j] < 0.025f) {
                            matrix[i][j] -= 0.01f;
                        }
                        if (tmpMatrix[i][j] > 0.1) {

                            float noiseNumber = Math.abs(random.nextFloat() * 10);

                            if (getNeighborCount(i, j) >= 6) {
                                matrix[i][j] = 0;
                            }
                            if (getNeighborCount(i, j) == 1 && frameCount % 4 == 1) {
                                //determines the reproduction location
                                switch(getNeighborCount(i,j)){
                                    case 1:
                                        if (matrix[i - spread - random.nextInt(spreadDeviation)][j] < reproductionThreshold) {
                                            matrix[i - spread - random.nextInt(spreadDeviation)][j] = 3f;
                                        }
                                    case 2:
                                        if (matrix[i][j - spread - random.nextInt(spreadDeviation)] < reproductionThreshold) {
                                            matrix[i][j - spread - random.nextInt(spreadDeviation)] = 3f;
                                        }
                                    case 3:
                                        if (matrix[i + spread + random.nextInt(spreadDeviation)][j] < reproductionThreshold) {
                                            matrix[i + spread + random.nextInt(spreadDeviation)][j] = 3f;
                                        }
                                    case 4:
                                        if (matrix[i][j + spread + random.nextInt(spreadDeviation)] < reproductionThreshold) {
                                            matrix[i][j + spread + random.nextInt(spreadDeviation)] = 3f;
                                        }
                                }
                            }

                        }
                        matrix[i][j] -= 0.005 + random.nextFloat() / 50;

                    }
                }

            }
        }

    }
    public int RGBToHex(int r,int g,int b){
        int rh = r << 16;
        int gh = g << 8;
        int bh = b;
        return rh | gh | bh;
    }

    public void mouseDragged(){
        matrix[mouseX][mouseY] = 1.0f;
    }
    public int getNeighborCount(int posX,int posY){
        int neighborCount = 0;
        if(matrix[posX+1][posY+1] > 0.1){
            neighborCount++;
        }
        if(matrix[posX-1][posY-1] > 0.1){
            neighborCount++;
        }
        if(matrix[posX+1][posY-1]  > 0.1){
            neighborCount++;
        }
        if(matrix[posX-1][posY+1]  > 0.1){
            neighborCount++;
        }
        if(matrix[posX+1][posY] > 0.1){
            neighborCount++;
        }
        if(matrix[posX][posY+1]  > 0.1){
            neighborCount++;
        }
        if(matrix[posX-1][posY] > 0.1){
            neighborCount++;
        }
        if(matrix[posX][posY-1] > 0.1){
            neighborCount++;
        }
        return neighborCount;

    }
}
